import React from "react";
import HeadNavigation from "./components/HeadNavigation";
import MainContent from "./components/MainContent";
import FooterBottom from "./components/FooterBottom";
import AboutPaprika from "./components/AboutPaprika";
import WhoIsPaprika from "./components/WhoIsPaprika";

function App() {
  return ( 
    <div>
    <h1 style={warna}> Paprika </h1>
    <HeadNavigation/>
    <MainContent/>
    <FooterBottom/>
    <p>Warna paprika ada tiga</p>
    <AboutPaprika/>
    <WhoIsPaprika/>
    </div>
  )
}
const warna = {
  color:"rgba(59,89,153,1)"
}

export default App;